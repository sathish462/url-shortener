-- Create the url_mapping table

CREATE TABLE IF NOT EXISTS url_mapping (
        id UUID NOT NULL CONSTRAINT URL_MAPPING_PKEY PRIMARY KEY,
        original_url VARCHAR(2083) NOT NULL,
        created_at TIMESTAMP NOT NULL
    );

-- Create an index on the original_url column
CREATE INDEX idx_original_url ON url_mapping (original_url);
