package com.dkb.urlshortener.repository

import com.dkb.urlshortener.entity.UrlMapping
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.UUID

/**
 * Repository interface for managing URL mappings.
 * Provides methods for CRUD operations on {@link UrlMapping} entities.
 */
@Repository
interface UrlMappingRepository : JpaRepository<UrlMapping, UUID> {
    /**
     * Retrieves the {@link UrlMapping} entity associated with the specified UUID.
     *
     * @param uuid The UUID of the URL mapping.
     * @return The {@link UrlMapping} entity if found, otherwise {@code null}.
     */
    fun getUrlMappingById(uuid: UUID): UrlMapping?

    /**
     * Retrieves the {@link UrlMapping} entity associated with the specified original URL.
     *
     * @param originalUrl The original URL for which to retrieve the mapping.
     * @return The {@link UrlMapping} entity if found, otherwise {@code null}.
     */
    fun getUrlMappingByOriginalUrl(originalUrl: String): UrlMapping?
}
