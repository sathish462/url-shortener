package com.dkb.urlshortener.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.LocalDateTime
import java.util.UUID

@Entity
@Table(name = "url_mapping")
class UrlMapping(
    @Id
    val id: UUID = UUID.randomUUID(),
    @Column(name = "original_url")
    val originalUrl: String,
    @Column(name = "created_at")
    val createdAt: LocalDateTime = LocalDateTime.now(),
) {
    /**
     * No-arguments constructor required by JPA
     */
    constructor() : this(
        id = UUID.randomUUID(),
        originalUrl = "",
        createdAt = LocalDateTime.now(),
    )
}
