package com.dkb.urlshortener.entity

object UrlMappingFactory {
    fun createUrlMappingFrom(originalUrl: String = "www.google.com"): UrlMapping =
        UrlMapping(
            originalUrl = originalUrl,
        )
}
