package com.dkb.urlshortener.exception

data class ErrorResponse(
    val status: Int,
    val error: String?,
)
