package com.dkb.urlshortener.exception

class ResourceNotFoundException(message: String) : RuntimeException(message)
