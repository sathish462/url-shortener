package com.dkb.urlshortener.exception

class InvalidRequestException(message: String) : RuntimeException(message)
