package com.dkb.urlshortener.service

import com.dkb.urlshortener.entity.UrlMapping
import java.util.UUID

interface UrlMappingService {
    fun createMapping(originalUrl: String): UrlMapping

    fun getUrlMappingByOriginalUrl(originalUrl: String): UrlMapping?

    fun getUrlMappingByIdentifier(identifier: UUID): UrlMapping?
}
