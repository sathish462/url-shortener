package com.dkb.urlshortener.service

import com.dkb.urlshortener.entity.UrlMapping
import com.dkb.urlshortener.entity.UrlMappingFactory
import com.dkb.urlshortener.exception.InvalidRequestException
import com.dkb.urlshortener.repository.UrlMappingRepository
import org.springframework.stereotype.Service
import java.util.UUID

/**
 * Service class responsible for managing URL mappings.
 */
@Service
class UrlMappingServiceImplementation(
    private val urlMappingRepository: UrlMappingRepository,
) : UrlMappingService {
    /**
     * Creates a new URL mapping for the given original URL.
     * @param originalUrl The original URL to be mapped.
     * @return The created URL mapping.
     */
    override fun createMapping(originalUrl: String): UrlMapping {
        if (this.getUrlMappingByOriginalUrl(originalUrl) != null) {
            throw InvalidRequestException("URL mapping already exists for URL : '$originalUrl'")
        } else {
            val mapping = UrlMappingFactory.createUrlMappingFrom(originalUrl = originalUrl)
            val savedMapping = urlMappingRepository.save(mapping)
            return savedMapping
        }
    }

    /**
     * Retrieves the URL mapping for the given original URL.
     * @param originalUrl The original URL to search for.
     * @return The URL mapping for the given original URL.
     */
    override fun getUrlMappingByOriginalUrl(originalUrl: String): UrlMapping? = urlMappingRepository.getUrlMappingByOriginalUrl(originalUrl)

    /**
     * Retrieves the URL mapping for the given identifier.
     * @param identifier The identifier of the URL mapping to retrieve.
     * @return The URL mapping for the given identifier.
     */
    override fun getUrlMappingByIdentifier(identifier: UUID): UrlMapping? = urlMappingRepository.getUrlMappingById(identifier)
}
