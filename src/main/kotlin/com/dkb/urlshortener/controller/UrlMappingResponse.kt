package com.dkb.urlshortener.controller

import java.util.UUID

data class UrlMappingResponse(
    val identifier: UUID,
    val url: String,
)
