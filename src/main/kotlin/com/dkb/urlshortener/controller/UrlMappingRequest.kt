package com.dkb.urlshortener.controller

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern

data class UrlMappingRequest(
    @field:NotBlank(message = "URL must not be null or empty")
    @field:Pattern(
        regexp = "^(?:(?:https?|ftp)://)?(?:www\\.)?[^\\s/\$.?#].\\S*\$",
        message = "Invalid URL format. Please provide a valid URL",
    )
    val url: String,
)
