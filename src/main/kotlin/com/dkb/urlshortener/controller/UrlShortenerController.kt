package com.dkb.urlshortener.controller

import com.dkb.urlshortener.exception.ResourceNotFoundException
import com.dkb.urlshortener.service.UrlMappingService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/dkb")
class UrlShortenerController(
    private val urlMappingService: UrlMappingService,
) {
    @Operation(summary = "Creates a URL mapping", description = "Creates a URL mapping for the given URL")
    @ApiResponses(
        ApiResponse(responseCode = "400", description = "Bad Request"),
        ApiResponse(responseCode = "500", description = "Internal Server Error"),
        ApiResponse(
            responseCode = "201",
            description = "URL mapping created successfully",
            content = [
                Content(
                    mediaType = "application/json",
                    schema = Schema(implementation = UrlMappingResponse::class),
                ),
            ],
        ),
    )
    @PostMapping("/url/createMapping")
    fun createMapping(
        @Valid @RequestBody body: UrlMappingRequest,
    ): ResponseEntity<UrlMappingResponse> {
        val response = urlMappingService.createMapping(body.url)
        return ResponseEntity.status(HttpStatus.CREATED).body(
            UrlMappingResponse(
                identifier = response.id,
                url = response.originalUrl,
            ),
        )
    }

    @Operation(summary = "Retrieves mapping by identifier", description = "Retrieves a URL mapping by its identifier")
    @ApiResponses(
        ApiResponse(responseCode = "404", description = "Mapping not found"),
        ApiResponse(responseCode = "500", description = "Internal Server Error"),
        ApiResponse(
            responseCode = "200",
            description = "Mapping found",
            content = [
                Content(
                    mediaType = "application/json",
                    schema = Schema(implementation = UrlMappingResponse::class),
                ),
            ],
        ),
    )
    @GetMapping("/retrieve-mapping-by-identifier")
    fun getMappingByIdentifier(
        @RequestParam identifier: UUID,
    ): ResponseEntity<UrlMappingResponse> {
        val response = urlMappingService.getUrlMappingByIdentifier(identifier = identifier)
        return if (response != null) {
            ResponseEntity.ok(
                UrlMappingResponse(
                    identifier = response.id,
                    url = response.originalUrl,
                ),
            )
        } else {
            throw ResourceNotFoundException("Mapping not found for identifier: '$identifier'")
        }
    }

    @Operation(summary = "Retrieves mapping by URL", description = "Retrieves a URL mapping by its URL")
    @ApiResponses(
        ApiResponse(responseCode = "404", description = "Mapping not found"),
        ApiResponse(responseCode = "500", description = "Internal Server Error"),
        ApiResponse(
            responseCode = "200",
            description = "Mapping found",
            content = [
                Content(
                    mediaType = "application/json",
                    schema = Schema(implementation = UrlMappingResponse::class),
                ),
            ],
        ),
    )
    @GetMapping("/retrieve-mapping-by-url")
    fun getMappingByUrl(
        @RequestParam("url") url: String,
    ): ResponseEntity<UrlMappingResponse> {
        if (url.isBlank()) {
            throw IllegalArgumentException("URL must not be null or empty")
        }
        val response = urlMappingService.getUrlMappingByOriginalUrl(originalUrl = url)
        return if (response != null) {
            ResponseEntity.ok(
                UrlMappingResponse(
                    identifier = response.id,
                    url = response.originalUrl,
                ),
            )
        } else {
            throw ResourceNotFoundException("Mapping not found for URL: '$url'")
        }
    }
}
