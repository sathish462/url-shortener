package com.dkb.urlshortener.controller

import com.dkb.urlshortener.entity.UrlMapping
import com.dkb.urlshortener.service.UrlMappingService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import java.time.LocalDateTime
import java.util.UUID

@ExtendWith(SpringExtension::class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = ["spring.main.allow-bean-definition-overriding=true"],
)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Testcontainers
class UrlShortenerControllerIntegrationTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var urlMappingService: UrlMappingService

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    init {
        postgreSQLContainer.start()
    }

    companion object {
        private const val POSTGRES_PORT = 5432

        @Container
        val postgreSQLContainer: PostgreSQLContainer<*> =
            PostgreSQLContainer<Nothing>(
                DockerImageName.parse("postgres:latest"),
            ).apply {
                withDatabaseName("testdb")
                withUsername("testuser")
                withPassword("testpassword")
                withExposedPorts(POSTGRES_PORT)
                setPortBindings(listOf("$POSTGRES_PORT:$POSTGRES_PORT"))
            }
    }

    val originalUrl = "https://dkb-codeChallange.com"
    val identifier: UUID = UUID.randomUUID()

    @Test
    fun `test createMapping endpoint`() {
        val responseJson = objectMapper.writeValueAsString(UrlMappingResponse(identifier, originalUrl))
        `when`(urlMappingService.createMapping(originalUrl)).thenReturn(
            UrlMapping(
                identifier,
                originalUrl,
                LocalDateTime.now(),
            ),
        )

        mockMvc.perform(
            post("/dkb/url/createMapping")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"url\": \"$originalUrl\"}"),
        )
            .andExpect(status().isCreated)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(responseJson))
    }

    @Test
    fun `test getMappingByIdentifier endpoint`() {
        val responseJson = objectMapper.writeValueAsString(UrlMappingResponse(identifier, originalUrl))

        `when`(urlMappingService.getUrlMappingByIdentifier(identifier)).thenReturn(
            UrlMapping(identifier, originalUrl, LocalDateTime.now()),
        )

        mockMvc.perform(get("/dkb/retrieve-mapping-by-identifier?identifier=$identifier"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(responseJson))
    }

    @Test
    fun `test getMappingByUrl endpoint`() {
        val responseJson = objectMapper.writeValueAsString(UrlMappingResponse(identifier, originalUrl))

        `when`(urlMappingService.getUrlMappingByOriginalUrl(originalUrl)).thenReturn(
            UrlMapping(identifier, originalUrl, LocalDateTime.now()),
        )

        mockMvc.perform(get("/dkb/retrieve-mapping-by-url?url=$originalUrl"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(responseJson))
    }
}
