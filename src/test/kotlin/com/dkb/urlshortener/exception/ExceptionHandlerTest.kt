package com.dkb.urlshortener.exception

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.springframework.http.HttpStatus
import org.springframework.web.context.request.WebRequest

class ExceptionHandlerTest {
    private val exceptionHandler = ExceptionHandler()

    @Test
    fun testHandleUncaughtException() {
        val error = Exception("Unexpected error")
        val request = mock(WebRequest::class.java)
        val expectedErrorResponse = ErrorResponse(status = 500, error = "Unexpected error")

        val response = exceptionHandler.handleUncaughtException(error, request)

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.statusCode)
        assertEquals(response.body, expectedErrorResponse)
    }

    @Test
    fun testHandleInvalidRequestException() {
        val error = InvalidRequestException("Invalid request")
        val request = mock(WebRequest::class.java)
        val expectedErrorResponse = ErrorResponse(status = 400, error = "Invalid request")

        val response = exceptionHandler.handleInvalidRequestException(error, request)
        assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
        assertEquals(response.body, expectedErrorResponse)
    }

    @Test
    fun testHandleResourceNotFoundException() {
        val error = ResourceNotFoundException("Resource not found")
        val request = mock(WebRequest::class.java)
        val expectedErrorResponse = ErrorResponse(status = 400, error = "Resource not found")

        val response = exceptionHandler.handleResourceNotFoundException(error, request)
        assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
        assertEquals(response.body, expectedErrorResponse)
    }
}
