package com.dkb.urlshortener.entity

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class UrlMappingTest {
    @Test
    fun `should be created with valid attributes`() {
        val beforeCreationDate = LocalDateTime.now()
        val urlMapping = UrlMappingFactory.createUrlMappingFrom()

        SoftAssertions.assertSoftly {
            assertThat(urlMapping.id).isNotNull
            assertThat(urlMapping.createdAt).isBetween(beforeCreationDate, LocalDateTime.now())
            assertThat(urlMapping.originalUrl).isEqualTo("www.google.com")
        }
    }
}
