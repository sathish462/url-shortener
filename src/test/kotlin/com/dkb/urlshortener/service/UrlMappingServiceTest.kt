package com.dkb.urlshortener.service

import com.dkb.urlshortener.entity.UrlMappingFactory
import com.dkb.urlshortener.repository.UrlMappingRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.clearInvocations
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.whenever

class UrlMappingServiceTest {
    private val urlMappingRepository: UrlMappingRepository = Mockito.mock()

    private val service: UrlMappingServiceImplementation = UrlMappingServiceImplementation(urlMappingRepository)

    private val originalUrl = "www.dkb.com"

    @AfterEach
    fun reset() {
        clearInvocations(urlMappingRepository)
    }

    @Test
    fun `should create and return url mapping`() {
        // Given
        val urlMapping = UrlMappingFactory.createUrlMappingFrom(originalUrl)
        Mockito.`when`(urlMappingRepository.save(any())).thenReturn(urlMapping)
        // When
        val urlMappingCreationResponse =
            service.createMapping(
                originalUrl,
            )
        // Then
        assertThat(urlMappingCreationResponse).isEqualTo(urlMapping)
    }

    @Test
    fun `should return urlMapping when searched by url`() {
        // Given
        val urlMapping = UrlMappingFactory.createUrlMappingFrom(originalUrl)
        whenever(urlMappingRepository.getUrlMappingByOriginalUrl(originalUrl)).doReturn(urlMapping)
        // When
        val retrievedMapping = service.getUrlMappingByOriginalUrl(originalUrl)

        // Then
        assertThat(retrievedMapping).isEqualTo(urlMapping)
    }

    @Test
    fun `should return Null when searched by non existing url`() {
        // Given
        whenever(urlMappingRepository.getUrlMappingByOriginalUrl(originalUrl)).doReturn(null)
        // When
        val retrievedMapping = service.getUrlMappingByOriginalUrl(originalUrl)

        // Then
        assertThat(retrievedMapping).isNull()
    }

    @Test
    fun `should return urlMapping when searched by identifier`() {
        // Given
        val urlMapping = UrlMappingFactory.createUrlMappingFrom(originalUrl)
        whenever(urlMappingRepository.getUrlMappingById(urlMapping.id)).doReturn(urlMapping)
        // When
        val retrievedMapping = service.getUrlMappingByIdentifier(urlMapping.id)

        // Then
        assertThat(retrievedMapping).isEqualTo(urlMapping)
    }

    @Test
    fun `should return null when searched by non existing identifier`() {
        // Given
        val urlMapping = UrlMappingFactory.createUrlMappingFrom(originalUrl)
        whenever(urlMappingRepository.getUrlMappingById(urlMapping.id)).doReturn(null)
        // When
        val retrievedMapping = service.getUrlMappingByIdentifier(urlMapping.id)

        // Then
        assertThat(retrievedMapping).isNull()
    }
}
