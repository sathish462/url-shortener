# URL Shortener

# DESIGN:

## 1. Task Description : Implement URL Shortening API

Develop a modern, production-ready API for a URL shortening service that allows users to shorten long URLs and resolve shortened URLs to their original form.

#### List of tasks/ Requirements:
1. Shorten URL Endpoint:
     * POST endpoint to shorten that accepts JSON and return identifier.
2. Resolve Shortened URL:
    * GET endpoint to return the original URL by identifier and vise versa.

## 2. Implementation

### Technology StackTrace
      
      - Source compatibility : Java 17
      - Kotlin : 1.9.22
      - Spring Boot : 3.2.2
      - Flyway
      - PostgreSQL
      - Mockito

### Functional Description: URL Shortener API

##### Endpoint 1: Create URL Mapping

- **URL:** `POST /dkb/url/createMapping`
  - **Description:** This endpoint allows users to create a URL mapping for a given long URL.
  - **Request Body:**
    ```json
    {
      "url": "string"
    }
    ```
     - `url` (string): The long URL to be shortened.

#### Responses:

- **201 Created:**
   - **Description:** URL mapping created successfully.
   - **Body Schema:**
     ```json
     {
       "identifier": "string",
       "url": "string"
     }
     ```
      - `identifier` (string): The generated identifier for the shortened URL.
      - `url` (string): The original long URL.

  - **400 Bad Request:**
     - **Description:** Invalid request body or URL format.

  - **500 Internal Server Error:**
     - **Description:** Unexpected error occurred during mapping creation.

##### Endpoint 2: Retrieve Mapping by Identifier

- **URL:** `GET /dkb/retrieve-mapping-by-identifier`
  - **Description:** This endpoint retrieves a URL mapping by its identifier.
  - **Query Parameters:**
     - `identifier` (UUID): The identifier of the shortened URL.

#### Responses:

- **200 OK:**
   - **Description:** Mapping found successfully.
   - **Body Schema:** Same as the response of the Create URL Mapping endpoint.

  - **404 Not Found:**
     - **Description:** Mapping not found for the provided identifier.

  - **500 Internal Server Error:**
     - **Description:** Unexpected error occurred during mapping retrieval.

##### Endpoint 3: Retrieve Mapping by URL

- **URL:** `GET /dkb/retrieve-mapping-by-url`
  - **Description:** This endpoint retrieves a URL mapping by its original long URL.
  - **Query Parameters:**
     - `url` (string): The original long URL.

#### Responses:

- **200 OK:**
   - **Description:** Mapping found successfully.
   - **Body Schema:** Same as the response of the Create URL Mapping endpoint.

  - **404 Not Found:**
     - **Description:** Mapping not found for the provided URL.

  - **400 Bad Request:**
     - **Description:** URL parameter is null or empty.

  - **500 Internal Server Error:**
     - **Description:** Unexpected error occurred during mapping retrieval.

### Database Tables
    URL_MAPPING ( ID, ORIGINAL_URL , CREATED_AT);

### Documentation
 using openapi-stater-webmvc - swagger OpenAPI documentation generated based on your application's code.
 
# How to build?
First, of course, clone the project (Used gitlab)

**In your console terminal:**
```shell
  ./gradlew clean build
```
This will build the application with Gradle and run the tests.

**In the IDE (IntelliJ):**

**1.** Open IntelliJ IDEA, select `File` -> `Open`, and choose your Gradle project directory. Click `Open`.

**2.** After the project is imported, go to File -> Settings (`IntelliJ IDEA` -> `Preferences` on macOS), 
then select `Build, Execution, Deployment` -> `Build Tools` -> `Gradle`.
Set both `Build` and `run` using and Run tests using to Gradle. Also, ensure the correct JDK is selected.

That's it. You should be able to work now in url-shortener project from IntelliJ.

**Note**: after code changes if the build fails because of code structure or unused imports use below command.
```shell
./gradlew ktlintformat
```
Ktlint is included to enforce coding style and standards

# How to run locally?
**In your console terminal:**
Make sure in application.properties when env you want to run `dev` is set as default
* dev
* sandbox

```shell
  docker-compose up
```
That's it the above command will start up the Postgres and the application. 

**In the IDE (IntelliJ):**
Make sure the DB is up by just running the docker postgres service.
Simply `run` the _UrlShortenerApplication.kt_.
Now you should be able to access the service in your `http://localhost:8080`.

# Monitoring
The application includes `spring actuator` dependency that enables management endpoints in the API. 
All the default enabled endpoints are accessible.

Eg: 
```shell
http://localhost:8091/actuator/health
```

# Multi-environment-configuration
The application can be deployed in two different env `dev` and `sandbox` using `.env` defined in docker compose.
Based on the `spring.profiles.active` profile the application is capable of referring to specific environment configuration.

## Another approach 
By using the helm with all properties defined and executing it by `.sh` in the pipeline.
By this approach multiple environment can be deployed as pods in K8s.


# Further improvements
### Production ready 
1. AOP - logging to the endpoints
2. Monitoring and Alerting - Integrate Prometheus(alerting and metrics),grafanna(visualize metrics) and sentry(for notification)
3. caching - DB caching when creating mapping use @cache-put by key and during retrieval use @Cacheable 

Above was not implemented because of time constraint.